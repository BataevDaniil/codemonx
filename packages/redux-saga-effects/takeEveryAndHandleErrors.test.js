import { runSaga } from 'redux-saga'
import { put } from 'redux-saga/effects'

import {
  takeEveryAndHandleErrors,
  createEffectMiddlewareTakeEveryAndHandleErrors,
} from './takeEveryAndHandleErrors'

describe('takeEveryAndHandleErrors', () => {
  test('test', async () => {
    const error = new Error('error')
    const rootSaga = function*() {
      yield takeEveryAndHandleErrors('pattern', function*() {
        console.log('start')
        throw error
      })
      yield put({ type: 'pattern' })
    }
    await runSaga(
      {
        dispatch: actions => actions,
        effectMiddlewares: [
          createEffectMiddlewareTakeEveryAndHandleErrors({
            anyError: function*(e) {
              console.log(e)
              expect(e).toEqual(error)
            },
          }),
        ],
      },
      rootSaga,
    ).toPromise()
  })
})
