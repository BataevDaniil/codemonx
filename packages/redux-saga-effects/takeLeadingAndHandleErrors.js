import { takeLeading } from 'redux-saga/effects'

import { handleErrors } from '../handleErrors'

const TAKE_LEADING_AND_HANDLE_ERRORS = 'TAKE_LEADING_AND_HANDLE_ERRORS'

export const takeLeadingAndHandleErrors = (pattern, saga, ...args) => ({
  type: TAKE_LEADING_AND_HANDLE_ERRORS,
  payload: {
    pattern,
    fn: saga,
    args,
  },
})

export const createEffectMiddlewareTakeLeadingAndHandleErrors = optionHandleErrors => next => effect => {
  if (effect && effect.type === TAKE_LEADING_AND_HANDLE_ERRORS) {
    const {
      payload: { pattern, fn, args },
    } = effect
    next(takeLeading(pattern, handleErrors(optionHandleErrors)(fn), ...args))
  } else next(effect)
}
