import { takeEvery } from 'redux-saga/effects'

import { handleErrors } from '../handleErrors'

const TAKE_EVERY_AND_HANDLE_ERRORS = 'TAKE_EVERY_AND_HANDLE_ERRORS'

export const takeEveryAndHandleErrors = (pattern, saga, ...args) => ({
  type: TAKE_EVERY_AND_HANDLE_ERRORS,
  payload: {
    pattern,
    fn: saga,
    args,
  },
})

export const createEffectMiddlewareTakeEveryAndHandleErrors = optionHandleErrors => next => effect => {
  if (effect && effect.type === TAKE_EVERY_AND_HANDLE_ERRORS) {
    const {
      payload: { pattern, fn, args },
    } = effect
    next(takeEvery(pattern, handleErrors(optionHandleErrors)(fn), ...args))
  } else next(effect)
}
