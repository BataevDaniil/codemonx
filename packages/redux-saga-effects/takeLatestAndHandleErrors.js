import { takeLatest } from 'redux-saga/effects'

import { handleErrors } from '../handleErrors'

const TAKE_LATEST_AND_HANDLE_ERRORS = 'TAKE_LATEST_AND_HANDLE_ERRORS'

export const takeLatestAndHandleErrors = (pattern, saga, ...args) => ({
  type: TAKE_LATEST_AND_HANDLE_ERRORS,
  payload: {
    pattern,
    fn: saga,
    args,
  },
})

export const createEffectMiddlewareTakeLatestAndHandleErrors = optionHandleErrors => next => effect => {
  if (effect && effect.type === TAKE_LATEST_AND_HANDLE_ERRORS) {
    const {
      payload: { pattern, fn, args },
    } = effect
    next(takeLatest(pattern, handleErrors(optionHandleErrors)(fn), ...args))
  } else next(effect)
}
