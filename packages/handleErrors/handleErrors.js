import * as R from 'ramda'
import { call } from 'redux-saga/effects'

export class RequestError extends Error {
  constructor(message, status, error) {
    super(message)
    this.status = parseInt(status)
    this.error = error
    this.name = this.constructor.name
  }
}

export class WebSocketError extends Error {
  constructor(message, error) {
    super(message)
    this.error = error
    this.name = this.constructor.name
  }
}

export const handleStatuses = () => res => {
  if (200 <= res.status && res.status < 300) {
    return Promise.resolve(res)
  }

  return Promise.reject(new RequestError('Request Error', res.status))
}

export const handleErrors = ({
  anyError,
  networkError,
  webSocketError,
  ...rest
} = {}) => saga =>
  function*(...args) {
    try {
      return yield call(saga, ...args)
    } catch (e) {
      let onceHandleError = false
      if (process.env.NODE_ENV !== 'production') {
        // eslint-disable-next-line
        console.warn('Saga error: ', e)
      }

      if (e instanceof RequestError) {
        for (const key in rest) {
          const statuses = R.pipe(
            R.split(','),
            R.map(parseInt),
          )(key)

          if (R.includes(e.status, statuses)) {
            yield call(rest[key], e, ...args)
            onceHandleError = true
          }
        }
      }

      if (
        networkError &&
        (e.message.indexOf('Network request failed') !== -1 ||
          e.message.indexOf('Failed to fetch') !== -1)
      ) {
        yield call(networkError, e, ...args)
        onceHandleError = true
      }

      // TODO: typing test
      if (webSocketError && e instanceof WebSocketError) {
        yield call(webSocketError, e, ...args)
        onceHandleError = true
      }

      if (anyError) {
        yield call(anyError, e, ...args)
        onceHandleError = true
      }
      if (onceHandleError) {
        return e
      }
      throw e
    }
  }
