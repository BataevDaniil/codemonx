import { runSaga } from 'redux-saga'

import { handleErrors, RequestError } from './handleErrors'

describe('util handleErrors', () => {
  test('Where handle one error or more then return error', async () => {
    const error = new RequestError('error', 400)
    const fWithError = () => {
      throw error
    }
    const result = await runSaga(
      {},
      handleErrors({ '400': function*() {}, anyError: function*() {} })(
        fWithError,
      ),
    ).toPromise()
    expect(result).toBe(error)
  })
  test('Where no handler has triggered cause the error to occur again', done => {
    const error = new Error('error')
    const fWithError = () => {
      throw error
    }
    runSaga(
      {
        onError(e) {
          expect(e).toBe(error)
          done()
        },
      },
      handleErrors()(fWithError),
    )
  })
  test('When saga completed without error return value saga', async () => {
    const returnValue = 10
    const f = () => returnValue
    const result = await runSaga({}, handleErrors()(f)).toPromise()
    expect(result).toBe(returnValue)
  })
  test('Handler network error', () => {
    const fWithError = () => {
      throw new Error('Network request failed')
    }

    const mockNetworkError = jest.fn()
    runSaga(
      {},
      handleErrors({
        networkError: function*() {
          mockNetworkError()
        },
      })(fWithError),
    )
    expect(mockNetworkError.mock.calls.length).toBe(1)
  })
  test('Handle certain statuses errors', () => {
    const fWithError = () => {
      throw new RequestError('error message', '400')
    }

    const mockNumberOneError = jest.fn()
    const mockNumberTwoError = jest.fn()
    runSaga(
      {},
      handleErrors({
        '400': function*() {
          mockNumberOneError()
        },
        '401': function*() {
          mockNumberTwoError()
        },
      })(fWithError),
    )
    expect(mockNumberOneError.mock.calls.length).toBe(1)
    expect(mockNumberTwoError.mock.calls.length).toBe(0)
  })
  test('One handler for many statuses errors', () => {
    const fWithStatusError = status => () => {
      throw new RequestError('error message', status)
    }
    let mockStatusError
    const handlerErrorWithConfig = handleErrors({
      '400,401,404': function*() {
        mockStatusError()
      },
    })

    mockStatusError = jest.fn()
    runSaga({}, handlerErrorWithConfig(fWithStatusError(400)))
    expect(mockStatusError.mock.calls.length).toBe(1)

    mockStatusError = jest.fn()
    runSaga({}, handlerErrorWithConfig(fWithStatusError(401)))
    expect(mockStatusError.mock.calls.length).toBe(1)

    mockStatusError = jest.fn()
    runSaga({}, handlerErrorWithConfig(fWithStatusError(404)))
    expect(mockStatusError.mock.calls.length).toBe(1)
  })
  test('When params anyError called on any error', () => {
    {
      const fWithError = () => {
        throw new Error('Network request failed')
      }
      const mockAnyError = jest.fn()
      const mockNumberOneError = jest.fn()
      const mockNumberTwoError = jest.fn()
      const mockNetworkError = jest.fn()
      runSaga(
        {},
        handleErrors({
          anyError: function*() {
            mockAnyError()
          },
          '400': function*() {
            mockNumberOneError()
          },
          '401': function*() {
            mockNumberTwoError()
          },
          networkError: function*() {
            mockNetworkError()
          },
        })(fWithError),
      )
      expect(mockAnyError.mock.calls.length).toBe(1)
      expect(mockNumberOneError.mock.calls.length).toBe(0)
      expect(mockNumberTwoError.mock.calls.length).toBe(0)
      expect(mockNetworkError.mock.calls.length).toBe(1)
    }
    {
      const fWithError = () => {
        throw new RequestError('error message', '400')
      }
      const mockAnyError = jest.fn()
      const mockNumberOneError = jest.fn()
      const mockNumberTwoError = jest.fn()
      const mockNetworkError = jest.fn()
      runSaga(
        {},
        handleErrors({
          anyError: function*() {
            mockAnyError()
          },
          '400': function*() {
            mockNumberOneError()
          },
          '401': function*() {
            mockNumberTwoError()
          },
          networkError: function*() {
            mockNetworkError()
          },
        })(fWithError),
      )
      expect(mockAnyError.mock.calls.length).toBe(1)
      expect(mockNumberOneError.mock.calls.length).toBe(1)
      expect(mockNumberTwoError.mock.calls.length).toBe(0)
      expect(mockNetworkError.mock.calls.length).toBe(0)
    }
  })
})
