import * as R from 'ramda'
import { eventChannel, END } from 'redux-saga'
import { take, cancelled, put, call, all } from 'redux-saga/effects'
import io from 'socket.io-client'
import { WebSocketError } from 'codemonx/packages/handleErrors'

const META_SOCKET_ON = 'socket/on'
const META_SOCKET_EMIT = 'socket/emit'
const prefixAction = (evenName, namespace) =>
  `SOCKET${namespace || ''}/${evenName}`

export const createActionForWebSocketOn = ({ eventName, namespace }) => {
  const type = prefixAction(eventName, namespace)
  const action = payload => ({
    type,
    payload,
    meta: META_SOCKET_ON,
  })
  action.toString = () => type

  return action
}

export const createActionForWebSocketEmit = ({ eventName, namespace }) => {
  const type = prefixAction(eventName, namespace)
  const action = payload => ({
    type,
    payload,
    meta: META_SOCKET_EMIT,
  })
  action.toString = () => type

  return action
}

const eventChannelSocket = (socket, eventNames, namespace) => {
  const unsubscribe = () => socket.close()

  return eventChannel(emitter => {
    eventNames.forEach(eventName =>
      socket.on(eventName, data =>
        emitter(createActionForWebSocketOn({ eventName, namespace })(data)),
      ),
    )
    socket.on('disconnect', () => emitter(END))
    return unsubscribe
  })
}

// namespace transfer how props path. Example "{ path: '/chat' }"
const reduxSagaWebSocket = function* ({ eventNames, domain, ...options }) {
  let channelSocket

  try {
    const socket = yield call(io, `${domain}${options.path}`)

    channelSocket = yield call(
      eventChannelSocket,
      socket,
      eventNames,
      options.path,
    )
    const putFromChannelSocket = function* () {
      while (true) {
        const action = yield take(channelSocket)
        yield put(action)
      }
    }
    const emitFromAction = function* () {
      while (true) {
        const { meta, payload, type } = yield take('*')
        if (meta === META_SOCKET_EMIT) {
          const eventName = R.pipe(
            R.split('/'),
            R.last,
          )(type)

          yield call([socket, socket.emit], eventName, payload)
        }
      }
    }
    yield all([putFromChannelSocket(), emitFromAction()])
  } catch (e) {
    channelSocket && channelSocket.close()
    throw e
  } finally {
    if (yield cancelled()) {
      channelSocket && channelSocket.close()
    } else {
      throw WebSocketError(`${options.path || ''}/disconnect`)
    }
  }
}

export default reduxSagaWebSocket
