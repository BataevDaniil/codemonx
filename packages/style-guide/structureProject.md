# stack

1.  ramda
2.  redux
3.  redux-actions
4.  react-router-dom
5.  redux-form
6.  redux-saga
7.  redux-persist
8.  reselect
9.  styled-components
10. Joi
11. React

# structure project

- [/src](#src)
  - [/modules](#modules)
    - [/name-module-1](#name-module-1)
      - [/constants.js](#module-constants)
      - [/index.js](#module-index)
      - [/Request.js](#request)
      - [/Repository.js](#repository)
      - [/sagas.js](#sagas)
      - [/state.js](#state)
      - [/utils.js](#module-utils)
    - [/rootReducer.js](#root-reducer)
    - [/rootSaga.js](#root-saga)
    - [/store.js](#store)
  - [/utils](#utils)
  - [/view](#view)
    - [/common](#common)
      - [/index.js](#common-index)
    - [/fonts](#fonts)
      - [/index.css](#fonts-index)
    - [/images](#images)
      - [/index.js](#images-index)
    - [/layouts](#layouts)
      - [/index.js](#layouts-index)
    - [/screens](#screens)
      - [/index.js](#screens-index)
  - [/App](#app)
  - [/constants](#constants)

### /name-module-1

1. Название модуля через дифис потому что все модули в npm так называются.
2. Каждый модуль должен отвечать за независимую часть приложения например `address` или `order`
   или `product`. Единственно от чего он может зависить это от [utils](#utils)

#### /module-constants

Содержит название модуля и константы для модуля которыми пользуются несколько файлов в модуле или они доступны
из модуля.

#### /module-index

```js
// описываем то что будет доступно из модуля
import * as _Request from './Request'
import * as _Repository from './Repository'

export const Request = _Request
export const Repository = _Repository

export {
  fetchAddAddress,
  updateAddress,
  replaceAddress,
  getAddress,
  getAddressId,
  fetchDateDelivery,
  getDateDelivery,
  getPriceDelivery,
} from './state'
export { default } from './state'

export { default as addressSaga } from './sagas'
// возможно еще константы или утилиты или еще что-то что будет использоваться вне
```

#### /Request

Содержит в себе методы для запросов на сервер. Каждый метод может включать в себя: преобразование
дынных, комбинцию запросов.

Например

```js
import * as R from 'ramda'
import { handleStatuses } from 'codemonx/packages/handleErrors'

import { pipeP } from '../../utils'

// имя хоста берется из .env
const withHost = url => `${process.env.REACT_APP_HOST}${url}`

// делаем такой вызов функции чтобы получить авокомплит когда будем писать запрос
// и передаев первых параметром объект
export const orders = ({ token }) =>
  // это R.pipeWith(R.then)
  pipeP(
    () =>
      fetch(withHost('/order'), {
        method: 'GET',
        headers: {
          Authorization: `Bearer ${token}`,
        },
      }),
    //
    handleStatuses(),
    x => x.json(),
    // преобразуем запрос
    R.map(({ Price, ...other }) => ({ price: Price, ...other })),
  )()

// функция для преобразования нескольких запросов
const mapStatistics = R.map(({ id, ...other }) => ({ other }))

// сдесь можно просто вызвать pipeP потому что не передаем не каких параметров
export const statisticsByOrders = pipeP(
  () =>
    fetch(withHost('/statistics?by=order'), {
      method: 'GET',
    }),
  handleStatuses(),
  x => x.json(),
  mapStatistics,
)

export const statisticsByVisits = pipeP(
  () =>
    fetch(withHost('/statistics?by=visits'), {
      method: 'GET',
    }),
  handleStatuses(),
  x => x.json(),
  mapStatistics,
)

// комбинируем запросы
export const statistics = pipeP(
  () => Promise.all(statisticsByOrders(), statisticsByVisits()),
  R.flatten,
)
```

#### /Repository

Содержит в себе методы для взаимодействия с хранилищем. Сейчас с `localStorage` в будущем если будет опыт с `indexDB`
то будут описано как и с ним работать.

```js
import { withModuleName } from './constants'

export const getOrder = () =>
  // не забываем JSON.parse
  JSON.parse(localStorage.getItem(withModuleName('/order')))

export const setOrder = order =>
  // не забываем JSON.stringify
  localStorage.setItem(withModuleName('/order'), JSON.stringify(order))
```

#### /sagas

Например

```js
import { handleErrors } from 'codemonx/packages/handleErrors'
import { takeLatest, call } from 'redux-saga/effects'

import { login } from './state'

// используем как один объект у которого есть много методов
import * as Request from './Request'
import * as Repository from './Repository'

// в большенстве случаем название саги бдует совпадать с названием экшина
// поэтому саге добавляем в конце Saga
const loginSaga = handleErrors({
  anyError: function*() {
    yield put(stopSubmitings)
  },
  '401': function*() {
    yield put(isLoading(false))
  },
})(function*({ payload: { username, password } }) {
  const token = yield call(Request.token, { username, password })
  yield call(Repository.setToken, token)
})

const checkAuthorization = function*() {}

// название соги совпадает с название модуля + Saga
const authorizationSaga = function*() {
  // сага которая должна выполнится когда приложение только запущено
  yield call(checkAuthorization)
  // сдесь запускаем все саги для одного модуля
  yield takeLatest(login, loginSaga)
}

export default authorizationSaga
```

#### /state

```js
import { createAction, handleAction, handleActions } from 'redux-actions'
import { combineReducers } from 'redux'
import storage from 'redux-persist/lib/storage'
import { persistReducer } from 'redux-persist'

import { MODULE_NAME } from './constants'

const createActionWithPrefix = type => createAction(`${MODULE_NAME}/${type}`)

// для получения части стора за который отвечаем этот модуль
const getAddress = R.prop('address')

// разделительная черта
//==============================================================================

// пишем экшины относящиеся к item
// пишем fetch если этот экшин будет вызвать сагу которая сделает запрос к сервера
export const fetchAddAddress = createActionWithPrefix('FETCH_ADD_ADDRESS')
export const replaceAddress = createActionWithPrefix('REPLACE_ADDRESS')
export const updateAddress = createActionWithPrefix('UPDATE_ADDRESS')
// deleteAddress
// if by id then deleteAddressById

// пишем item
export const item = handleActions(
  {
    [replaceAddress]: (_, { payload }) => payload,
    [updateAddress]: (address, { payload: other }) => ({
      ...other,
      ...address,
    }),
  },
  {},
)

// пишем селекторы item
export const getItems = R.pipe(
  getAddress,
  R.prop('item'),
)

export const getAddressId = R.pipe(
  getItem,
  R.prop('id'),
)

// разделяем state
//==============================================================================

export const fetchDateDelivery = createActionWithPrefix('FETCH_DATE_DELIVERY')
export const setDelivery = createActionWithPrefix('SET_DELIVERY')

const delivery = handleAction(setDelivery, (_, { payload }) => payload, {})

const getDelivery = R.pipe(
  getAddress,
  R.prop('delivery'),
)

export const getDateDelivery = R.pipe(
  getDelivery,
  R.prop('date'),
)

export const getPriceDelivery = R.pipe(
  getDelivery,
  R.prop('price'),
)

// разделительная черта
//==============================================================================

// address название модуля
const address = combineReducers({ item, delivery })

const persistConfig = {
  // address название модуля
  key: 'address',
  storage: storage,
  // говорим какой state модуля будет сохраняться при изменении
  whitelist: ['item', 'delivery'],
}

export default persistReducer(persistConfig, address)
```

#### /module-utils

Утилиты модуля.

#### /root-reducer

Собирает все редьюсеры из модулей в одну

Например

```js
import { combineReducers } from 'redux'

import ui from './ui'
import parcel from './parcel'

const rootReducer = combineReducers({
  ui,
  parcel,
})

export default rootReducer
```

### /root-saga

Собирает все саги из модулей в одну и запускает их.

Например

```js
import { navigationSaga } from './navigation'
import { parcelSaga } from './parcel'

const rootSaga = function*() {
  yield all([navigationSaga(), parcelSaga()])
}

export default rootSaga
```

### /store

Конфигурирует store

## /utils

Утилиты приложения.

## /view

Содержит все что касается отображения.

### /common

Содержит общие компоненты хотябы для двух экранов или двух layout

#### /common-index

Экспортирует то что должно быть доступно вне

## /fonts

Содержит все шрифты

### /fonts-index

Подключается все шрифты.

### /images

Содержит все картинки.

#### /images-index

Экспортирует то что должно быть доступно вне в одном объекте.

Например

```js
import image1 from './image1'
import image2 from './image2'

const images = {
  image1,
  image2,
}

export default images
```

## /layouts

Рендерит взависимости от урла нужный экран или несколько комнонентов экранов. И при этому если нужно
рундирит общую часть некоторых экранов.

Если у layout нет своих комнонентов то это один js файл если есть свои компоненты тогда создаем папку также
как и с экранами

### /layouts-index

Экспорт по умолчанию один `Root.js` в котором рендирится все по логике вашего приложения.

## /screens

Один экрна может быть одним файлом (например `Order.js`) или если это большой экран который можно разделить
на компоненты то это будет

```
/Order
  /Component1.js
  /Component2.js
  /Order.js <- гланый комонент то есть сам экран
  /index.js <- экспортит Order.js по умолчанию
```

Если экран нельзя предствить одним компоненом вложенным в layout то экран должен экспортить по умолчанию один
объект в котором будут лежать эти компоненты чтобы layout их вставил в нужные месте.

Например

```js
import Component1 from './Component1'
import Component2 from './Component2'

const Order = {
  Component1,
  Component2,
}

export default Order
```

Нужно думать об экране как об одном или наборе компоненты нужных экрану.

### /screens-index

Экспорт всех экранов

## /app

Предоставлет провайдеры приложению и рендерит `Root.js` из layout, и импортит шрифты.

## /constants

Содержит константы для нзвание форм и роутов.

Например

```js
export const ROUTES = {
  BASE: '/',
  PARCEL_INFO: '/parcel-info',
  DELIVERY: '/delivery',
  CONFIRMATION: '/confirmation',
  PAYMENT: '/payment',
}

export const FORMS = {
  DELIVER_PARCEL: 'deliverParcel',
  TRACK_CODE: 'trackCode',
  BUSINESS: 'business',
}
```
