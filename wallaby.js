module.exports = function(wallaby) {
  return {
    files: [
      'packages/**/*.js',
      'jest.config.js', // <--
      '!packages/**.test.js',
    ],
    env: {
      type: 'node',
      runner: 'node',
    },
    tests: ['packages/**.test.js'],

    testFramework: 'jest',

    compilers: {
      'packages/**/*.js': wallaby.compilers.babel(),
    },
  }
}
